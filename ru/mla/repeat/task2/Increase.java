package ru.mla.repeat.task2;

import java.util.Arrays;
import java.util.Scanner;

public class Increase {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double[] array = {10.57, 22.928, -9.8, 47.11, 51.36, 89.1, 0.8489, -5.515};
        System.out.print("Какой элемент вы хотите увеличить(введите номер элемента): ");
        int element = scanner.nextInt() - 1;
        if (element + 1 > array.length) {
            System.out.println("Чисел в массиве: " + array.length + ", вы вышли за пределы массива");

        } else {

            System.out.println("Изначально: " + Arrays.toString(array));

            increase(element, array);
            System.out.println("После изменения: " + Arrays.toString(array));
        }
    }

    private static void increase(int element, double[] array) {
        array[element] += array[element] * 0.1;
    }
}