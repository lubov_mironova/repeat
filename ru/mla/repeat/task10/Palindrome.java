package ru.mla.repeat.task10;

import java.util.Scanner;

public class Palindrome {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите строку: ");
        String line = scanner.nextLine();

        if (isPalindrome(line)) {
            System.out.println("Строка является палиндромом");
        } else {
            System.out.println("Строка не является палиндромом");
        }
    }

    private static boolean isPalindrome(String line) {
        String workLineOne = line.replaceAll("[\\s()?:!.,;{}\"-]", "");
        String workLineTwo = new StringBuffer(workLineOne).reverse().toString();
        boolean retVal = workLineOne.equalsIgnoreCase(workLineTwo);
        return retVal;
    }
}
