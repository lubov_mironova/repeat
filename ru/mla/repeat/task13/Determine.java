package ru.mla.repeat.task13;

import java.util.Scanner;

public class Determine {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите один символ: ");
        String line = scanner.nextLine();

        char symbol = line.charAt(0);
        if (Character.isDigit(symbol)) System.out.println("Это цифра");
        if (Character.isLetter(symbol)) System.out.println("Это буква");
        if (".,:;-".contains(line)) System.out.println("Это знак пунктуации");
    }
}
