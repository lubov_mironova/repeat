package ru.mla.repeat.task14;

import java.util.Scanner;

public class Matrix {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Количество строк: ");
        int line = scanner.nextInt();
        System.out.print("Количество колонок: ");
        int column = scanner.nextInt();

        int[][] matrix = new int[line][column];
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < column; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
            }
        }
        for (int i = 0; i < line; i++, System.out.println()) {
            for (int j = 0; j < column; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }
        System.out.println("Транспанированная матрица:");
        for (int i = 0; i < column; i++, System.out.println()) {
            for (int j = 0; j < line; j++) {
                System.out.print(matrix[j][i] + " ");
            }
        }
    }
}
