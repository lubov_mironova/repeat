package ru.mla.repeat.task3;
import java.util.Scanner;

public class Currency {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите количество рублей: ");
        double rubles = scanner.nextDouble();
        System.out.print("Введите курс евро: ");
        double euro = scanner.nextDouble();
        System.out.println(rubles +" рублей = " + euro + " евро");
    }

    private static double euro(double rate, double rubles){
        return rubles/rate;
    }
}
