package ru.mla.repeat.task11;

import java.util.Scanner;

public class Time {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите количество суток: ");
        int days = scanner.nextInt();
        System.out.println("Часов: "+days*24+" \n Минут: "+days*24*60+" \n Секунд: "+days*24*60*60);
    }
}
