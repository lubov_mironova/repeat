package ru.mla.repeat.task7;

import java.util.Scanner;

public class Number {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        double numb = scanner.nextDouble();

        if (numb % 1 == 0){
            System.out.println("Введенное число - целое");
        } else {
            System.out.println("Введенное число - дробное");
        }
    }
}
