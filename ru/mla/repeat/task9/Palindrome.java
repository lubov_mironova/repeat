package ru.mla.repeat.task9;

import java.util.Scanner;

public class Palindrome {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        double numb = scanner.nextDouble();

        if (isPalendrom(numb)) {
            System.out.println("Число является палиндромом");
        } else {
            System.out.println("Число не является палиндромом");
        }
    }

    private static boolean isPalendrom(double numb) {
        String workLine = String.valueOf(numb);
        if (numb % 1 == 0) {
            workLine = String.valueOf((int)numb);
        }
        String workLineOne = workLine.replaceAll("[\\s()?:!.,;{}\"-]", "");
        String workLineTwo = new StringBuffer(workLineOne).reverse().toString();
        boolean retVal = workLineOne.equalsIgnoreCase(workLineTwo);
        return retVal;
    }
}
